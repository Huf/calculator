$(document).ready(function() {

  var calculator = {
    equationField: $('#equation'),
    memory: [],
    state: 'cleared',
    total: 0,
  };

  // assign data-btn-val as button value
  $('[data-btn-val]').each(function() {
    $(this).text($(this).attr('data-btn-val'));
  });

  // focus on equationField resets to zero
  if($(calculator.equationField).is(':focus')) {
    calculator.equationField.val('0');
  }

  // Make draggable
  $('.calc').draggable();

  // click event for Number Buttons
  $('.calc--buttons__number').click(function(e) {
    e.preventDefault();

    // if(calculator.memory.length > 0) {
    //
    //   }
    //   calculator.equationField.val(btnVal);
    // }

    var btnVal = $(this).attr('data-btn-val');

    // update equation field
    if (calculator.equationField.val() === '0') {
      calculator.equationField.val(btnVal);
    } else if (calculator.equationField.val().indexOf('.') === -1){
      calculator.equationField.val(calculator.equationField.val() + btnVal);
    } else {
      calculator.equationField.val(calculator.equationField.val() + btnVal);
    }

    // console.log(calculator.memory);

  });

  // Clear button
  $('#calc--buttons__clear').click(function(e) {
    e.preventDefault();
    calculator.equationField.val('0');
    calculator.memory = [];
    calculator.total = 0;
  });

  // Plus button
  $('#calc--buttons__plus').click(function(e) {
    e.preventDefault();

    // Add last value
    calculator.memory.push(parseInt(calculator.equationField.val()));
    calculator.memory.push('+');
  });

  // Minus button
  $('#calc--buttons__minus').click(function(e) {
    e.preventDefault();

    calculator.memory.push(calculator.equationField.val());
    calculator.memory.push('-');
  });

  // Multiply button
  $('#calc--buttons__multiply').click(function(e) {
    e.preventDefault();

    // Add last value
    calculator.memory.push(parseInt(calculator.equationField.val()));
    calculator.memory.push('*');
  });

  // Divide button
  $('#calc--buttons__divide').click(function(e) {
    e.preventDefault();

    // Add last value
    calculator.memory.push(parseInt(calculator.equationField.val()));
    calculator.memory.push('/');
  });

  // Decimal button
  $('#calc--buttons__decimal').click(function(e) {
    e.preventDefault();

    // var btnVal = $(this).attr('data-btn-val');
    //
    // calculator.equationField.val(calculator.equationField.val() + btnVal);


    // Add last value
    calculator.memory.push(parseInt(calculator.equationField.val()));
    calculator.memory.push('.');
  });

  // Equals button
  $('#calc--buttons__equals').click(function(e) {
    e.preventDefault();

    // Push current value to memory
    calculator.memory.push(parseInt(calculator.equationField.val()));
    calculator.total = calculator.memory.join(" ");
    calculator.total = eval(calculator.total);
    calculator.equationField.val(calculator.total);
    calculator.memory = [];

  });

  window.addEventListener("keyup", checkKeyPressed, false);

  function checkKeyPressed(e) {
    // If equationField is in focus, we can type numbers in directly instead of triggering a click
    if(calculator.equationField.is(':focus')) {
      return;
    }

    // Numbers 0-9
    for(var i = 48; i < 58; i++) {
      if (e.keyCode == [i]) {
        $('[data-btn-val="' + ([i] - 48) + '"]').trigger('click');
      }
    }

    // Plus key
    if (e.which == 107) {
      $('[data-btn-val="+"]').trigger('click');
    }

    // Minus key
    if (e.which == 109) {
      $('[data-btn-val="-"]').trigger('click');
    }

    // Enter (or equals) key
    if (e.which == 13 || e.keyCode == 187) {
      $('[data-btn-val="="]').trigger('click');
    }

    // Clear key (cmd + c)
    if (e.keyCode == 67) {
      console.log('cmd + c');
      $('[data-btn-val="C"]').trigger('click');
    }
  }

});
